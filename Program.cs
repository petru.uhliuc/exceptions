﻿using System;
using System.Threading.Tasks;

namespace exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            Random ran = new Random();
            int waiting = ran.Next(1000, 5000);
            StatieMeteo statie = new StatieMeteo();
            Task<int> a = statie.getTemp(waiting);
            bool b = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }
            waiting = ran.Next(1000, 5000);
            StatieMeteo statie1 = new StatieMeteo();
            Task<int> a1 = statie1.getTemp(waiting);
            bool b1 = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b1 = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }
            waiting = ran.Next(1000, 5000);
            StatieMeteo statie2 = new StatieMeteo();
            Task<int> a2 = statie2.getTemp(waiting);
            bool b2 = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b2 = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }
            waiting = ran.Next(1000, 5000);
            StatieMeteo statie3 = new StatieMeteo();
            Task<int> a3 = statie3.getTemp(waiting);
            bool b3 = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b3 = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }
            waiting = ran.Next(1000, 5000);
            StatieMeteo statie4 = new StatieMeteo();
            Task<int> a4 = statie4.getTemp(waiting);
            bool b4 = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b4 = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }
            waiting = ran.Next(1000, 5000);
            StatieMeteo statie5 = new StatieMeteo();
            Task<int> a5 = statie5.getTemp(waiting);
            bool b5 = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b5 = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }
            waiting = ran.Next(1000, 5000);
            StatieMeteo statie6 = new StatieMeteo();
            Task<int> a6 = statie6.getTemp(waiting);
            bool b6 = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b6 = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }
            waiting = ran.Next(1000, 5000);
            StatieMeteo statie7 = new StatieMeteo();
            Task<int> a7 = statie7.getTemp(waiting);
            bool b7 = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b7 = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }
            waiting = ran.Next(1000, 5000);
            StatieMeteo statie8 = new StatieMeteo();
            Task<int> a8 = statie8.getTemp(waiting);
            bool b8 = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b8 = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }
            waiting = ran.Next(1000, 5000);
            StatieMeteo statie9 = new StatieMeteo();
            Task<int> a9 = statie9.getTemp(waiting);
            bool b9 = true;
            try
            {
                if (ran.Next(100) < 25)
                {
                    Exception e = new Exception();
                    b9 = false;
                    throw e;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Serviciu indisponibil");

            }

            if (b)
                Console.WriteLine("Station has detected the temperature " + a.Result);
            if (b1)
                Console.WriteLine("Station has detected the temperature " + a1.Result);
            if (b2)
                Console.WriteLine("Station has detected the temperature " + a2.Result);
            if (b3)
                Console.WriteLine("Station has detected the temperature " + a3.Result);
            if (b4)
                Console.WriteLine("Station has detected the temperature " + a4.Result);
            if (b5)
                Console.WriteLine("Station has detected the temperature " + a5.Result);
            if (b6)
                Console.WriteLine("Station has detected the temperature " + a6.Result);
            if (b7)
                Console.WriteLine("Station has detected the temperature " + a7.Result);
            if (b8)
                Console.WriteLine("Station has detected the temperature " + a8.Result);
            if (b9)
                Console.WriteLine("Station has detected the temperature " + a9.Result);
        }
    }

    public class StatieMeteo
    {
        private readonly Random random;
        private int temp;

        public StatieMeteo()
        {
            random = new Random();
        }

        public async Task<int> getTemp(int timer)
        {
            int result = await Task.Run(() => random.Next(20, 30));
            Task.Delay(timer).Wait();
            temp = result;
            return result;
        }

        public void Print()
        {
            Console.WriteLine("Station has detected the temperature " + this.temp);
        }
    }
}
